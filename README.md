# ansible-netdc-interfaces

This role is responsible for configuring FRR, /etc/network/interfaces and some
Cumulus specific settings on network switches running Cumulus Linux.

Naturalis uses this role together with a private inventory.

## Requirements

This role is meant to be run on network switches running a recent version of
Cumulus Linux. The role is tested on Cumulus Linux 3.7.12.

## Role Variables

To isolate networks and routing tables you can define multiple tenants
and tenant networks. For every tenant one or more VRF can be defined. Every VRF
consists of VLAN's with corresponding subnets.

For every tenant, port configurations can be defined. A port configuration
defines the tagged and untagged VLAN's on a switchport among some other
configuration parameters. See the example below:

```yaml
tenants:
  - name: openstack
    vrfs:
      - name: management
        id: 10 # Used to name vrf (e.g. vrf10)
        vlans:
          - name: ipmi
            vid: 1001 # The vid should be unique within the network.
            subnet: 10.180.0.0/23
          - name: maas
            vid: 1002
            subnet: 10.180.2.0/23
        l3_vni: 4001
      - name: cluster
        id: 11
        vlans:
          - name: external
            vid: 1101
            subnet: 10.181.0.0/23
          - name: internal
            vid: 1102
            subnet: 10.181.2.0/23
          - name: provider
            vid: 1103
            subnet: 10.181.4.0/23
          - name: ceph
            vid: 1104
            subnet: 10.181.6.0/23
        l3_vni: 4002
    port_configs:
      stack:
        vrf: 11
        tagged:
          - 1101
          - 1102
          - 1103
      ceph:
        vrf: 11
        untagged: 1104
        mtu: 9000 # Overrides the default MTU size of 1500
      maas:
        vrf: 10
        untagged: 1001
        tagged:
          - 1002
        lacp_bypass: true # Configures LACP bypass on the port / bond
      pxe:
        vrf: 10
        untagged: 1002
        lacp_bypass: true
      ipmi:
        vrf: 10
        untagged: 1001
        balance_xor: true # Makes sure a bond functions without LACP config
```

To configure bonds for servers connected to the Cumulus switches use this
variable structure. In the (typical) case that a bond consists of links to two
different switches a CLAG ID will be generated. In all the cases a bond is
configured, also if there's just one bond member.

The `short_name` of the server has a maximum of 7 characters. It should be
unique within the rack and will be used to name the bond interfaces.

```yaml
servers:
  - name: openstack-hypervisor-c09a
    short_name: osh-a
    tenant: openstack # Name of the tenant from tenants list
    bonds:
      - port_config: public # Name of the port_config that's defined under tenant
        members:
          - switch: netdc-leaf-c09a
            ports: swp1
          - switch: netdc-leaf-c09b
            ports: swp2
      - port_config: mgmt
        members:
          - switch: netdc-leaf-c09a
            ports: swp2
          - switch: netdc-leaf-c09b
            ports: swp3
  - name: openstack-hypervisor-c09b
    short_name: osh-b
    tenant: openstack
    bonds:
      - port_config: public
        members:
          - switch: netdc-leaf-c09a
            ports: [swp2, swp3] # Define multiple ports on a switch as list
  - name: openstack-infra-c09a
    short_name: osi-a
    tenant: openstack
    bonds:
      - port_config: public
        members:
          - switch: netdc-leaf-c09a
            ports: swp2 # Or just one port
```

## Dependencies

The templates in this role make use of:

* The [jinja2 Expression Statement
    extension](https://jinja.palletsprojects.com/en/2.11.x/extensions/#expression-statement). You need to add this line in your `ansible.cfg`:

    `jinja2_extensions = jinja2.ext.do`

* The `ipaddr` [Jinja2 filter](https://docs.ansible.com/ansible/latest/user_guide/playbooks_filters_ipaddr.html). You need to install the `netaddr` Python package:

  `pip3 install --user netaddr`

## Example Playbook

You can apply this role with a playbook like this:

```
---
- hosts: switches
  gather_facts: false
  become: true
  tasks:
  - include_role:
      name: netdc-interfaces
```

## License

Apache version 2.0

## Author Information

Foppe Pieters
David Heijkamp
