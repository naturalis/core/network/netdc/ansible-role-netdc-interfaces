# ports.conf --
#
# This file controls port speed, aggregation and subdivision.
#
# The Accton AS7326-56XS has:
#
#     48 SFP28 ports numbered 1-48
#
#         These ports are 25G by default and are arranged in groups of four
#         ports in the switch chip.  If 10G is desired for a port, the entire
#         group of four ports must all be configured as 10G.  The following
#         table defines the arrangement of these groups of four ports:
#              1   2   3   6*
#              4   5   7*  9
#              8  10  11* 12
#             13  14  15  18*
#             16  17  19* 21
#             20  22  23* 24
#             25  26  27  30*
#             28  29  31* 33
#             32  34  35* 36
#             37  38  39  42*
#             40* 41  43  45
#             44* 46  47  48
#         If, for example, port 19 is configured for 10G, then ports 16, 17,
#         and 21 must also be configured for 10G.
#
#         Additionally, each group of four ports can be ganged together as a
#	  100G or 40G port.  When ganged together, one port -- based on the
#	  arrangement of the ports -- is designated as the gang leader.  This
#	  port's number is used to configure the ganged ports and is marked
#	  with an asterix (*) above.
#
#     8 QSFP28 ports numbered 49-56
#
#         These ports are configurable as 40G/50G/100G or split into 4x10G and
#         4x25G ports.

# SFP28 ports
#
# <port label 1-48> = [25G|10G|100G/4|40G/4]
1=10G
2=10G
3=10G
4=10G
5=10G
6=10G
7=10G
8=10G
9=10G
10=10G
11=10G
12=10G
13=10G
14=10G
15=10G
16=10G
17=10G
18=10G
19=10G
20=10G
21=10G
22=10G
23=10G
24=10G
25=10G
26=10G
27=10G
28=10G
29=10G
30=10G
31=10G
32=10G
33=10G
34=10G
35=10G
36=10G
37=10G
38=10G
39=10G
40=10G
41=10G
42=10G
43=10G
44=10G
45=10G
46=10G
47=10G
48=10G

# QSFP28 ports
#
# <port label 49-56> = [100G|50G|40G|4x25G|4x10G]
49=40G
50=40G
51=40G
52=40G
53=40G
54=40G
55=40G
56=40G
